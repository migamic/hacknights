// script.js
document.addEventListener("DOMContentLoaded", function () {
    const modalOverlay = document.getElementById("modal-overlay");
    const closeModalButton = document.getElementById("close-modal");
    const openModalButton = document.getElementById("open-modal-button");

    function openModal() {
        modalOverlay.style.display = "block";
        openModalButton.style.display = "none";
        setTimeout(() => {
            document.querySelector(".modal").style.top = "0";
        }, 10);
    }

    function closeModal() {
        document.querySelector(".modal").style.top = "-50px"; // Adjust to hide the modal
        setTimeout(() => {
            modalOverlay.style.display = "none";
            openModalButton.style.display = "block";
        }, 300); // The same duration as the CSS transition
    }

    openModalButton.addEventListener("click", openModal);
    closeModalButton.addEventListener("click", closeModal);
});

