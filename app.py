from flask import Flask, request, redirect

app = Flask(__name__)

@app.route('/submit', methods=['POST'])
def submit():
    input_field = request.form.get('mapSizeInput')

    print("Input Field:", input_field)

    return redirect('/game.html')

if __name__ == '__main__':
    app.run(debug=True)

