import random

MAZE_HEIGHT = 15
MAZE_WIDTH = 15

STARTING_ROW = 0
STARTING_COL = 0

def get_hex(cell):
    val = 0
    if cell['wallLeft']:
        val += 8
    if cell['wallTop']:
        val += 4
    if cell['wallRight']:
        val += 2
    if cell['wallBottom']:
        val += 1
    return hex(val)[2:]

def print_maze(maze):
    for i in maze:
        print(', '.join(map(get_hex,i)))


def initial_cell():
    cell = {
            'visited' : False,
            'wallLeft' : True,
            'wallTop' : True,
            'wallRight' : True,
            'wallBottom' : True
        }
    return cell


def init_maze(maze):
    for i in range(MAZE_HEIGHT):
        for j in range(MAZE_WIDTH):
            maze[i][j] = initial_cell()


def DFS(maze, pos):
    cell = maze[pos[0]][pos[1]]
    assert not cell['visited']
    cell['visited'] = True

    visit_order = [0,1,2,3]
    random.shuffle(visit_order)

    for i in visit_order:
        if i == 0: new_pos = (pos[0]-1, pos[1])
        elif i == 1: new_pos = (pos[0], pos[1]+1)
        elif i == 2: new_pos = (pos[0]+1, pos[1])
        elif i == 3: new_pos = (pos[0], pos[1]-1)

        if new_pos[0]<0 or new_pos[1]<0 or new_pos[0]>=MAZE_HEIGHT or new_pos[1]>=MAZE_WIDTH:
            continue # Out of bounds

        if not maze[new_pos[0]][new_pos[1]]['visited']:
            # Unvisited neighbour. Visit it and join
            new_cell = maze[new_pos[0]][new_pos[1]]
            if i == 0:
                cell['wallTop'] = False
                new_cell['wallBottom'] = False
            elif i == 1:
                cell['wallRight'] = False
                new_cell['wallLeft'] = False
            elif i == 2:
                cell['wallBottom'] = False
                new_cell['wallTop'] = False
            elif i == 3:
                cell['wallLeft'] = False
                new_cell['wallRight'] = False
            DFS(maze, new_pos)

    # All neighbours have been visited, return


def build_maze(maze):
    DFS(maze, (STARTING_ROW,STARTING_COL))


if __name__ == "__main__":
    maze = [[None] * MAZE_WIDTH for _ in range(MAZE_HEIGHT)]

    init_maze(maze)
    build_maze(maze)
    print_maze(maze)
