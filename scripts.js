// JavaScript to show and hide the modals
function showModal(modalId) {
    const modal = document.getElementById(modalId);
    modal.style.display = 'block';
}

function closeModal(modalId) {
    const modal = document.getElementById(modalId);
    modal.style.display = 'none';
}

// Close the modal if the user clicks outside of it
window.onclick = function(event) {
    const modals = document.querySelectorAll('.modal');
    modals.forEach(function(modal) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    });
}

// Function to handle form submission (Button 1)
function submitForm() {
    // Add your form submission logic here
    const mapSizeInput = document.getElementById('mapSizeInput').value;
    window.location.href = 'game.html';
}

