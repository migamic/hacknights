# A*maze*ing golf

A simple browser game where the user controlls a golf ball inside a procedurally-generated labyrinth. The goal is to reach a chest, found somewhere in the scene.

## Controls

The user can move forwards and backwards with the `up and down arrow` keys. The camera is rotated with the `left and right arrows`. In order to get a better view of the user's position within the maze, the ball will jump high into the air with `space`.

## Gameplay

The ball is spawned in a random position, and so is a chest. Whenever the user reaches the chest, the game resets into a new random setup.

## Installation and usage

Run the game in a local server with, e.g. `python -m http.server` from the root folder of this repo.

## Hacknights

This project was made by Malika Uteuliyeva & Jaume Ros Alonso during a UPC *hacknight*, a 8h hackathon, from 9pm to 5am.
