
// Set up scene, camera, and renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
const textureLoader = new THREE.TextureLoader();

// Labyrinth parameters
const WALL_HEIGHT = 3;
const WALL_WIDTH = 0.3;
const CUBE_SIZE = 3;
const LAB_ROWS = 15;
const LAB_COLS = 15;

// Load wall textures
const wallTexture = textureLoader.load('textures/brick_wall.jpg'); 
const topTexture = textureLoader.load('textures/cement.jpeg');
topTexture.minFilter = THREE.LinearFilter; // Disable mipmapping

const wallMaterial = new THREE.MeshBasicMaterial({ map: wallTexture });
const topMaterial = new THREE.MeshBasicMaterial({ map: topTexture });

var walls = []


function createMeshFromHex(hex, x, y, z) {
  const bin = parseInt(hex, 16).toString(2).padStart(4, '0');

  // Left Wall
  if (bin[0] === '1') {
    // Main wall body without the top
    const leftWallBody = new THREE.BoxGeometry(WALL_WIDTH, WALL_HEIGHT - 0.1, CUBE_SIZE);
    const leftWallBodyMesh = new THREE.Mesh(leftWallBody, wallMaterial);
    leftWallBodyMesh.position.set(x - CUBE_SIZE / 2 + WALL_WIDTH / 2, y + 0.05, z); // slight y adjustment
    scene.add(leftWallBodyMesh);
    walls.push(leftWallBodyMesh);

    // Top part of the wall
    const leftWallTop = new THREE.BoxGeometry(WALL_WIDTH, 0.1, CUBE_SIZE); // 0.1 as the height for the top
    const leftWallTopMesh = new THREE.Mesh(leftWallTop, topMaterial);
    leftWallTopMesh.position.set(x - CUBE_SIZE / 2 + WALL_WIDTH / 2, y + WALL_HEIGHT / 2, z); // position it on the top
    scene.add(leftWallTopMesh);
    walls.push(leftWallTopMesh);
  }

  // Top Wall
  if (bin[1] === '1') {
    // Main wall body without the top
    const topWallBody = new THREE.BoxGeometry(CUBE_SIZE, WALL_HEIGHT - 0.1, WALL_WIDTH);
    const topWallBodyMesh = new THREE.Mesh(topWallBody, wallMaterial);
    topWallBodyMesh.position.set(x, y + 0.05, z - CUBE_SIZE / 2 + WALL_WIDTH / 2); // slight y adjustment
    scene.add(topWallBodyMesh);
    walls.push(topWallBodyMesh);

    // Top part of the wall
    const topWallTop = new THREE.BoxGeometry(CUBE_SIZE, 0.1, WALL_WIDTH); // 0.1 as the height for the top
    const topWallTopMesh = new THREE.Mesh(topWallTop, topMaterial);
    topWallTopMesh.position.set(x, y + WALL_HEIGHT / 2, z - CUBE_SIZE / 2 + WALL_WIDTH / 2); // position it on the top
    scene.add(topWallTopMesh);
    walls.push(topWallTopMesh);
  }

  // Right Wall
  if (bin[2] === '1') {
    // Main wall body without the top
    const rightWallBody = new THREE.BoxGeometry(WALL_WIDTH, WALL_HEIGHT - 0.1, CUBE_SIZE);
    const rightWallBodyMesh = new THREE.Mesh(rightWallBody, wallMaterial);
    rightWallBodyMesh.position.set(x + CUBE_SIZE / 2 - WALL_WIDTH / 2, y + 0.05, z); // slight y adjustment
    scene.add(rightWallBodyMesh);
    walls.push(rightWallBodyMesh);

    // Top part of the wall
    const rightWallTop = new THREE.BoxGeometry(WALL_WIDTH, 0.1, CUBE_SIZE); // 0.1 as the height for the top
    const rightWallTopMesh = new THREE.Mesh(rightWallTop, topMaterial);
    rightWallTopMesh.position.set(x + CUBE_SIZE / 2 - WALL_WIDTH / 2, y + WALL_HEIGHT / 2, z); // position it on the top
    scene.add(rightWallTopMesh);
    walls.push(rightWallTopMesh);
  }

  // Bottom Wall
  if (bin[3] === '1') {
    // Main wall body without the top
    const bottomWallBody = new THREE.BoxGeometry(CUBE_SIZE, WALL_HEIGHT - 0.1, WALL_WIDTH);
    const bottomWallBodyMesh = new THREE.Mesh(bottomWallBody, wallMaterial);
    bottomWallBodyMesh.position.set(x, y + 0.05, z + CUBE_SIZE / 2 - WALL_WIDTH / 2); // slight y adjustment
    scene.add(bottomWallBodyMesh);
    walls.push(bottomWallBodyMesh);

    // Top part of the wall
    const bottomWallTop = new THREE.BoxGeometry(CUBE_SIZE, 0.1, WALL_WIDTH); // 0.1 as the height for the top
    const bottomWallTopMesh = new THREE.Mesh(bottomWallTop, topMaterial);
    bottomWallTopMesh.position.set(x, y + WALL_HEIGHT / 2, z + CUBE_SIZE / 2 - WALL_WIDTH / 2); // position it on the top
    scene.add(bottomWallTopMesh);
    walls.push(bottomWallTopMesh);
  }

}

function createLabyrinthFromCSV(data) {
  const rows = data.trim().split('\n');
  for (let i = 0; i < rows.length; i++) {
      const cells = rows[i].split(',');
      for (let j = 0; j < cells.length; j++) {
          const hexVal = cells[j].toUpperCase();
          createMeshFromHex(hexVal, j * CUBE_SIZE, WALL_HEIGHT/2.0, i * CUBE_SIZE);
      }
  }
}


fetch('maze2.csv')
  .then(response => response.text())
  .then(data => {
    createLabyrinthFromCSV(data);
});

// Create a sphere
const sphereGeometry = new THREE.SphereGeometry(0.5, 32, 32);
const sphereTexture = textureLoader.load('textures/golf_ball.jpg'); 
sphereTexture.minFilter = THREE.LinearFilter; // Disable mipmapping
const sphereMaterial = new THREE.MeshBasicMaterial({ map: sphereTexture });
const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
// Random position of a sphere
sphere.position.y = 0.5;
randomValueX = Math.floor(Math.random() * LAB_ROWS);
sphere.position.x +=randomValueX * CUBE_SIZE;
randomValueZ = Math.floor(Math.random() * LAB_COLS);
sphere.position.z +=randomValueZ * CUBE_SIZE;
scene.add(sphere);

// Test cube
const staticCubeGeometry = new THREE.BoxGeometry();
const staticCubeMaterial = new THREE.MeshBasicMaterial({ color: 0xFFAAAA });
const staticCube = new THREE.Mesh(staticCubeGeometry, staticCubeMaterial);
// Random position of a test cube
staticCube.position.y = 0.5;
randomValueX = Math.floor(Math.random() * LAB_ROWS);
staticCube.position.x +=randomValueX * CUBE_SIZE;
randomValueZ = Math.floor(Math.random() * LAB_COLS);
staticCube.position.z +=randomValueZ * CUBE_SIZE;
scene.add(staticCube);

// Create a floor
const floorGeometry = new THREE.PlaneGeometry((LAB_ROWS) * CUBE_SIZE, (LAB_COLS) * CUBE_SIZE, 100, 100);
const floorTexture = textureLoader.load('textures/grass2.jpg');
floorTexture.minFilter = THREE.LinearFilter; // Disable mipmapping
floorTexture.wrapS = THREE.RepeatWrapping;
floorTexture.wrapT = THREE.RepeatWrapping;
floorTexture.repeat.set(10, 10);
const floorMaterial = new THREE.MeshBasicMaterial({ map: floorTexture });
const floor = new THREE.Mesh(floorGeometry, floorMaterial);
floor.rotation.x = - Math.PI / 2; // Rotate the floor to make it horizontal
floor.position.x += (LAB_ROWS - 1) * CUBE_SIZE / 2.0;
floor.position.z += (LAB_COLS - 1) * CUBE_SIZE / 2.0;
scene.add(floor);

// Position the camera to follow the cube
const cameraDistance = 0.1;
const cameraHeight = 6;

// Set up arrow key controls
var currRotation = 0;
var currMove = 0;
const rotationSpeed = 0.05;
const moveSpeed = 0.07;

let isJumping = false;
let jumpVelocity = 0;

document.addEventListener('keydown', (event) => {
switch (event.key) {
  case 'ArrowUp':
        currMove = -1;
        break;
  case 'ArrowDown':
        currMove = 1;
        break;
  case 'ArrowLeft':
        currRotation = 1;
        break;
  case 'ArrowRight':
        currRotation = -1;
        break;
  case ' ':
        // Spacebar: initiate jump if not already jumping
        if (!isJumping) {
          isJumping = true;
          jumpVelocity = 1.0; // Adjust the jump height by changing this value
        }
        break;
  }
});

document.addEventListener('keyup', (event) => {
    switch (event.key) {
      case 'ArrowUp':
      case 'ArrowDown':
            currMove = 0;
        break;
      case 'ArrowLeft':
      case 'ArrowRight':
            currRotation = 0;
        break;
    }
});


function checkCollisions() {
    var sphereBoundingSphere = new THREE.Sphere();
    sphere.geometry.computeBoundingSphere();
    sphereBoundingSphere.copy(sphere.geometry.boundingSphere).applyMatrix4(sphere.matrixWorld);

    // Iterate through each cube and check for collisions
    for (var i = 0; i < walls.length; i++) {
        var wall = walls[i];

        // Create a bounding box for the wall
        var wallBoundingBox = new THREE.Box3().setFromObject(wall);

        // Check for intersection between the sphere's bounding sphere and wall's bounding box
        if (wallBoundingBox.intersectsSphere(sphereBoundingSphere)) {
            console.log("Collision detected with wall", i);

            // Find the closest point on the wall's bounding box to the sphere's center
            var closestPoint = wallBoundingBox.clampPoint(sphereBoundingSphere.center);

            // Calculate the displacement vector (from the closest point to the sphere's center)
            var displacement = new THREE.Vector3().subVectors(sphereBoundingSphere.center, closestPoint);
            var penetrationDepth = sphereBoundingSphere.radius - displacement.length();

            // Normalize the displacement and scale by the penetration depth
            displacement.normalize().multiplyScalar(penetrationDepth);
            displacement.y = 0;
            console.log(displacement);

            // Adjust the sphere's position based on the displacement
            sphere.position.add(displacement);

            // Recompute the bounding sphere after moving the sphere
            sphereBoundingSphere.copy(sphere.geometry.boundingSphere).applyMatrix4(sphere.matrixWorld);
        }
    }
}


// Animation loop
const animate = () => {
    requestAnimationFrame(animate);


    if (!isJumping) {
        sphere.translateZ(currMove * moveSpeed);
    }
    sphere.rotation.y += currRotation * rotationSpeed;

    checkCollisions();

    // Update the camera position to follow the sphere
    const sphereDirection = new THREE.Vector3(Math.sin(sphere.rotation.y), 0, Math.cos(sphere.rotation.y));
    camera.position.copy(sphere.position).add(sphereDirection.multiplyScalar(cameraDistance));
    camera.position.y += cameraHeight;
    camera.lookAt(sphere.position);

    // Handle jumping
    if (isJumping) {
      sphere.position.y += jumpVelocity;
      jumpVelocity -= 0.01; // Adjust the gravity effect by changing this value

      // Check if the sphere has landed
      if (sphere.position.y <= 0.5) {
        sphere.position.y = 0.5;
        isJumping = false;
      }
    }

    renderer.render(scene, camera);
};

animate();
