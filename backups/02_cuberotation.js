// Set up scene, camera, and renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Create a cube
const cubeGeometry = new THREE.BoxGeometry();
const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xBB99DD });
const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
cube.position.y = 0.5;
scene.add(cube);

// Test cube
const staticCubeGeometry = new THREE.BoxGeometry();
const staticCubeMaterial = new THREE.MeshBasicMaterial({ color: 0xFFAAAA });
const staticCube = new THREE.Mesh(staticCubeGeometry, staticCubeMaterial);
staticCube.position.z = -2;
staticCube.position.y = 0.5;
scene.add(staticCube);

// Create a floor
const floorGeometry = new THREE.PlaneGeometry(10, 10);
const floorMaterial = new THREE.MeshBasicMaterial({ color: 0x334433, side: THREE.DoubleSide });
const floor = new THREE.Mesh(floorGeometry, floorMaterial);
floor.rotation.x = Math.PI / 2; // Rotate the floor to make it horizontal
scene.add(floor);

// Position the camera
const cameraDistance = 5;
const cameraHeight = 2;
const cameraOffset = new THREE.Vector3(0, cameraHeight, -cameraDistance);
camera.position.copy(cube.position).add(cameraOffset);
camera.lookAt(cube.position);


// Set up arrow key controls
var currRotation = 0;
var currMove = 0;
const rotationSpeed = 0.03;
const moveSpeed = 0.1;
const moveDirection = new THREE.Vector3();

document.addEventListener('keydown', (event) => {
switch (event.key) {
  case 'ArrowUp':
        currMove = 1;
        break;
  case 'ArrowDown':
        currMove = -1;
        break;
  case 'ArrowLeft':
        currRotation = 1;
        break;
  case 'ArrowRight':
        currRotation = -1;
        break;
  }
});

document.addEventListener('keyup', (event) => {
    switch (event.key) {
      case 'ArrowUp':
      case 'ArrowDown':
            currMove = 0;
        break;
      case 'ArrowLeft':
      case 'ArrowRight':
            currRotation = 0;
        break;
    }
});

// Animation loop
const animate = () => {
    requestAnimationFrame(animate);

    cube.translateZ(currMove * moveSpeed);
    cube.rotateY(currRotation * rotationSpeed);

    // Update the camera position to follow the cube
    camera.position.copy(cube.position).add(cameraOffset);
    camera.lookAt(cube.position);

    renderer.render(scene, camera);
};

animate();
