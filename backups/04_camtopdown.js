// Set up scene, camera, and renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Create a cube
const cubeGeometry = new THREE.SphereGeometry(0.5, 32, 32);
const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xBB99DD });
const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
cube.position.y = 0.5;
scene.add(cube);

// Test cube
const staticCubeGeometry = new THREE.BoxGeometry();
const staticCubeMaterial = new THREE.MeshBasicMaterial({ color: 0xFFAAAA });
const staticCube = new THREE.Mesh(staticCubeGeometry, staticCubeMaterial);
staticCube.position.z = -2;
staticCube.position.y = 0.5;
scene.add(staticCube);

// Create a floor
const floorGeometry = new THREE.PlaneGeometry(10, 10);
const floorMaterial = new THREE.MeshBasicMaterial({ color: 0x334433, side: THREE.DoubleSide });
const floor = new THREE.Mesh(floorGeometry, floorMaterial);
floor.rotation.x = Math.PI / 2; // Rotate the floor to make it horizontal
floor.position.x += 5;
floor.position.z += 5;
scene.add(floor);

// Position the camera to follow the cube
const cameraDistance = 0.1;
const cameraHeight = 6;

// Set up arrow key controls
var currRotation = 0;
var currMove = 0;
const rotationSpeed = 0.05;
const moveSpeed = 0.07;

document.addEventListener('keydown', (event) => {
switch (event.key) {
  case 'ArrowUp':
        currMove = -1;
        break;
  case 'ArrowDown':
        currMove = 1;
        break;
  case 'ArrowLeft':
        currRotation = 1;
        break;
  case 'ArrowRight':
        currRotation = -1;
        break;
  }
});

document.addEventListener('keyup', (event) => {
    switch (event.key) {
      case 'ArrowUp':
      case 'ArrowDown':
            currMove = 0;
        break;
      case 'ArrowLeft':
      case 'ArrowRight':
            currRotation = 0;
        break;
    }
});

// CSV Parsing function
function parseCSV(data) {
  const rows = data.trim().split('\n');
  return rows.map(row => row.split(',').map(Number));
}

// Function to create the labyrinth
function createLabyrinth(data) {

  const geometry = new THREE.BoxGeometry(1, 3, 1);
  const material = new THREE.MeshBasicMaterial({color: 0x0000ff});

  for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data[i].length; j++) {
          if (data[i][j] === 1) {
              const cube = new THREE.Mesh(geometry, material);
              cube.position.set(i, 1.5, j);
              scene.add(cube);
          }
      }
  }
}

// Load the CSV and render the labyrinth
fetch('labyrinth.csv')
  .then(response => response.text())
  .then(data => {
      const parsedData = parseCSV(data);
      createLabyrinth(parsedData);
});

// Animation loop
const animate = () => {
    requestAnimationFrame(animate);

    cube.translateZ(currMove * moveSpeed);
    cube.rotation.y += currRotation * rotationSpeed;

    // Update the camera position to follow the cube
    const cubeDirection = new THREE.Vector3(Math.sin(cube.rotation.y), 0, Math.cos(cube.rotation.y));
    camera.position.copy(cube.position).add(cubeDirection.multiplyScalar(cameraDistance));
    camera.position.y += cameraHeight;
    camera.lookAt(cube.position);

    renderer.render(scene, camera);
};

animate();
