// Set up scene, camera, and renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Create a cube
const cubeGeometry = new THREE.SphereGeometry(0.5, 32, 32);
const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xBB99DD });
const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
cube.position.y = 0.5;
scene.add(cube);

// Test cube
const staticCubeGeometry = new THREE.BoxGeometry();
const staticCubeMaterial = new THREE.MeshBasicMaterial({ color: 0xFFAAAA });
const staticCube = new THREE.Mesh(staticCubeGeometry, staticCubeMaterial);
staticCube.position.z = -2;
staticCube.position.y = 0.5;
scene.add(staticCube);

// Create a floor
const floorGeometry = new THREE.PlaneGeometry(10, 10);
const floorMaterial = new THREE.MeshBasicMaterial({ color: 0x334433, side: THREE.DoubleSide });
const floor = new THREE.Mesh(floorGeometry, floorMaterial);
floor.rotation.x = Math.PI / 2; // Rotate the floor to make it horizontal
floor.position.x += 5;
floor.position.z += 5;
scene.add(floor);

// Position the camera to follow the cube
const cameraDistance = 0.1;
const cameraHeight = 6;

// Set up arrow key controls
var currRotation = 0;
var currMove = 0;
const rotationSpeed = 0.05;
const moveSpeed = 0.07;

document.addEventListener('keydown', (event) => {
switch (event.key) {
  case 'ArrowUp':
        currMove = -1;
        break;
  case 'ArrowDown':
        currMove = 1;
        break;
  case 'ArrowLeft':
        currRotation = 1;
        break;
  case 'ArrowRight':
        currRotation = -1;
        break;
  }
});

document.addEventListener('keyup', (event) => {
    switch (event.key) {
      case 'ArrowUp':
      case 'ArrowDown':
            currMove = 0;
        break;
      case 'ArrowLeft':
      case 'ArrowRight':
            currRotation = 0;
        break;
    }
});

const WALL_HEIGHT = 3;
const WALL_WIDTH = 0.3;
const CUBE_SIZE = 5;
const material = new THREE.MeshBasicMaterial({color: 0x0000ff});
function createMeshFromHex(hex, x, y, z) {
  const bin = parseInt(hex, 16).toString(2).padStart(4, '0');

  if (bin[0] === '1') {  // left wall
      const leftWall = new THREE.BoxGeometry(WALL_WIDTH, WALL_HEIGHT, CUBE_SIZE);
      const leftWallMesh = new THREE.Mesh(leftWall, material);
      leftWallMesh.position.set(x - CUBE_SIZE / 2 + WALL_WIDTH / 2, y, z);
      scene.add(leftWallMesh);
  }
  if (bin[1] === '1') {  // top wall
      const topWall = new THREE.BoxGeometry(CUBE_SIZE, WALL_HEIGHT, WALL_WIDTH);
      const topWallMesh = new THREE.Mesh(topWall, material);
      topWallMesh.position.set(x, y, z - CUBE_SIZE / 2 + WALL_WIDTH / 2);
      scene.add(topWallMesh);
  }
  if (bin[2] === '1') {  // right wall
      const rightWall = new THREE.BoxGeometry(WALL_WIDTH, WALL_HEIGHT, CUBE_SIZE);
      const rightWallMesh = new THREE.Mesh(rightWall, material);
      rightWallMesh.position.set(x + CUBE_SIZE / 2 - WALL_WIDTH / 2, y, z);
      scene.add(rightWallMesh);
  }
  if (bin[3] === '1') {  // bottom wall
      const bottomWall = new THREE.BoxGeometry(CUBE_SIZE, WALL_HEIGHT, WALL_WIDTH);
      const bottomWallMesh = new THREE.Mesh(bottomWall, material);
      bottomWallMesh.position.set(x, y, z + CUBE_SIZE / 2 - WALL_WIDTH / 2);
      scene.add(bottomWallMesh);
  }
}

function createLabyrinthFromCSV(data) {
  const rows = data.trim().split('\n');
  for (let i = 0; i < rows.length; i++) {
      const cells = rows[i].split(',');
      for (let j = 0; j < cells.length; j++) {
          const hexVal = cells[j].toUpperCase();
          createMeshFromHex(hexVal, j * CUBE_SIZE, 1.5, i * CUBE_SIZE);
      }
  }
}


fetch('maze1.csv')
  .then(response => response.text())
  .then(data => {
    createLabyrinthFromCSV(data);
});


// Animation loop
const animate = () => {
    requestAnimationFrame(animate);

    cube.translateZ(currMove * moveSpeed);
    cube.rotation.y += currRotation * rotationSpeed;

    // Update the camera position to follow the cube
    const cubeDirection = new THREE.Vector3(Math.sin(cube.rotation.y), 0, Math.cos(cube.rotation.y));
    camera.position.copy(cube.position).add(cubeDirection.multiplyScalar(cameraDistance));
    camera.position.y += cameraHeight;
    camera.lookAt(cube.position);

    renderer.render(scene, camera);
};

animate();
